#include "algorithms.h"
#include "rbTree.h"

#include <fstream>
#include <iostream>

using namespace RB;

int increase(rbTree& tree, int id, int count) {
  // replace with a single lookup after implementing iterators
  if (!tree.has(id)) {
    tree.insert(id, count);
    return count;
  } else {
    auto& countref = tree.at(id);
    countref += count;
    return countref;
  }
}

int reduce(rbTree& tree, int id, int count) {
  // replace with a single lookup after implementing iterators
  if (!tree.has(id)) {
    return 0;
  } else if (auto& countref = tree.at(id); countref <= count) {
    tree.remove(id);
    return 0;
  } else {
    countref -= count;
    return countref;
  }
}

int main(int argc, char* argv[]) {
  if (argc != 2)  // ensure exactly once command line parameter is passed - to
                  // init the tree.
    throw "commandline arguments not passed properly";
  std::ifstream file(argv[1]);

  int count;
  file >> count;  // read count

  // we initialize and pass a lambda that outputs the readings in-order
  auto tree = build_tree(count, [&file]() -> std::pair<int, int> {
    int a, b;
    file >> a >> b;
    return std::make_pair(a, b);
  });

  std::string command;
  do {
    int a, b;
    std::cin >> command;
    if (command == "increase") {  // command is increase - we read two numbers
                                  // and call increase
      std::cin >> a >> b;
      std::cout << increase(tree, a, b) << std::endl;
    } else if (command == "reduce") {  // command is reduce - we read two
                                       // numbers and call reduce
      std::cin >> a >> b;
      std::cout << reduce(tree, a, b) << std::endl;
    } else if (command == "count") {  // command is count - we read one number
                                      // and call count
      std::cin >> a;
      std::cout << (tree.has(a) ? tree.at(a) : 0) << std::endl;
    } else if (command == "inrange") {  // command is inrange - we read two
                                        // numbers and call inrange
      std::cin >> a >> b;
      std::cout << sum_range(tree, a, b) << std::endl;
    } else if (command ==
               "next") {  // command is next - we read one number and call next
      std::cin >> a;
      auto temp = tree.next(a);
      std::cout << temp.first << " " << temp.second << std::endl;
    } else if (command == "previous") {  // command is previous - we read one
                                         // number and call previous
      std::cin >> a;
      auto temp = tree.previous(a);
      std::cout << temp.first << " " << temp.second << std::endl;
    }
  } while (command != "quit");  // command matched to quit - we quit
}
