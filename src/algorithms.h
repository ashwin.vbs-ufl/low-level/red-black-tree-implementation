#include <functional>

namespace RB {

class rbTree;

template <class T>
void swap(T& t1, T& t2) {
  T temp = t1;
  t1 = t2;
  t2 = temp;
}

// sum count values of all nodes inbetween id1 and id2 (including bound nodes)
int sum_range(rbTree& tree, int id1, int id2);

// builds a tree based on number of nodes.
// getset function returns the pairs in sorted order.
rbTree build_tree(int nodes, std::function<std::pair<int, int>()> getset);
};  // namespace RB