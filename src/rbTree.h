#include <utility>

namespace RB {

class rbTree {
 private:
  struct rbNode {
    int index;
    int count;

    bool isBlack;

    rbNode* left_child;
    rbNode* right_child;
    rbNode* parent;

    rbNode(int id, int m)
        : index(id),
          count(m),
          isBlack(false),
          left_child(nullptr),
          right_child(nullptr),
          parent(nullptr) {}
  };

  rbNode* root;
  std::pair<bool, rbNode*> locate(int id);

  rbNode* next(rbNode* node);
  rbNode* previous(rbNode* node);

  // return nodes with min and max id values
  rbNode* min(rbNode* node);
  rbNode* max(rbNode* node);

  rbNode* getBrother(rbNode* node);
  rbNode* getUncle(rbNode* node);
  rbNode* getGrandpa(rbNode* node);

  // checks only the first level fo children.
  int countRedChildren(rbNode* node);

  // rotates the tree around the given pivot node.
  void rotate(rbNode* pivot);

  void balancePostInsert(rbNode* node);
  void balancePostRemove(rbNode* node);

 public:
  // returns count value for node with key id
  bool has(int id);
  int& at(int id);

  // insert and remove nodes
  void insert(int id, int count);
  void remove(int id);

  // next and prev returns id and count values
  std::pair<int, int> next(int id);
  std::pair<int, int> previous(int id);

  rbTree() : root(nullptr) {}
};

}  // namespace RB
