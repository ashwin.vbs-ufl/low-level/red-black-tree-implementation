#include "rbTree.h"

using namespace RB;

rbTree::rbNode* rbTree::getUncle(rbNode* node) {
  if (node->parent != nullptr)
    return getBrother(node->parent);
  else
    return nullptr;
}

rbTree::rbNode* rbTree::getGrandpa(rbNode* node) {
  if (node->parent == nullptr)
    return nullptr;
  else
    return node->parent->parent;
}

rbTree::rbNode* rbTree::getBrother(rbNode* node) {
  if (node->parent == nullptr)
    return nullptr;

  if (node->parent->right_child == node)
    return node->parent->left_child;
  else
    return node->parent->right_child;
}

// Ref: https://en.wikipedia.org/wiki/Tree_rotation
void rbTree::rotate(rbNode* pivot) {
  auto base = pivot->parent;
  if (base == nullptr)
    throw "no parent at pivot";

  bool right_rotate = (base->left_child == pivot);
  if (right_rotate) {
    auto B = pivot->right_child;

    if (base->parent) {
      if (base->parent->right_child == base)
        base->parent->right_child = pivot;
      else
        base->parent->left_child = pivot;
    }

    pivot->right_child = base;
    pivot->parent = base->parent;

    base->parent = pivot;
    base->left_child = B;

    if (B)
      B->parent = base;
  } else {
    auto B = pivot->left_child;

    if (base->parent) {
      if (base->parent->right_child == base)
        base->parent->right_child = pivot;
      else
        base->parent->left_child = pivot;
    }

    pivot->left_child = base;
    pivot->parent = base->parent;

    base->parent = pivot;
    base->right_child = B;

    if (B)
      B->parent = base;
  }
  if (pivot->parent == nullptr)
    root = pivot;
}

void rbTree::insert(int id, int count) {
  auto temp = locate(id);
  if (temp.first)
    throw "node already exists?";
  if (temp.second == nullptr) {
    // no nodes in tree - insert as root and balance tree
    root = new rbNode(id, count);
    balancePostInsert(root);
  } else if (temp.second->index > id) {
    // insert node as left child and update tree
    temp.second->left_child = new rbNode(id, count);
    temp.second->left_child->parent = temp.second;
    balancePostInsert(temp.second->left_child);
  } else if (temp.second->index < id) {
    // insert node as right child and update tree
    temp.second->right_child = new rbNode(id, count);
    temp.second->right_child->parent = temp.second;
    balancePostInsert(temp.second->right_child);
  }
}

void rbTree::balancePostInsert(rbNode* node) {
  auto grandpa = getGrandpa(node);
  auto uncle = getUncle(node);

  // nodes inserted according to XYc rules given in lecture slides

  if (node->parent == nullptr)
    // case: node is root
    node->isBlack = true;
  else if (node->parent->isBlack)
    // case: parent is black
    return;
  else if (uncle && !uncle->isBlack) {
    // case: XYr
    node->parent->isBlack = true;
    uncle->isBlack = true;
    grandpa->isBlack = false;
    balancePostInsert(grandpa);
  } else if ((node == node->parent->left_child &&
              node->parent == grandpa->left_child) ||
             (node == node->parent->right_child &&
              node->parent == grandpa->right_child)) {
    //  case LLb or RRb
    node->parent->isBlack = true;
    grandpa->isBlack = false;
    rotate(node->parent);
  } else if ((node == node->parent->left_child &&
              node->parent == grandpa->right_child) ||
             (node == node->parent->right_child &&
              node->parent == grandpa->left_child)) {
    // case: RLb or LRb
    node->isBlack = true;
    grandpa->isBlack = false;
    rotate(node);
    rotate(node);
  }
}

void rbTree::remove(int id) {
  auto temp = locate(id);
  auto node = temp.second;

  if (!temp.first)  // node not found
    return;
  else if (node->left_child && node->right_child) {
    // case: node with degree 2
    auto pred = previous(node);
    // update the node to predecesspr's values and
    // update the node pointer to predecessor's
    node->index = pred->index;
    node->count = pred->count;
    node = pred;
  }

  if (node->left_child || node->right_child) {
    // case: current node has atleast one child => its a
    // black node with a red child
    auto child = node->left_child ? node->left_child : node->right_child;

    if (child != nullptr) {
      node->index = child->index;
      node->count = child->count;
      node->left_child = nullptr;
      node->right_child = nullptr;
      delete child;
    }
  } else if (!node->isBlack) {
    // case: node is a single red leaf
    if (node == node->parent->right_child)
      node->parent->right_child = nullptr;
    else
      node->parent->left_child = nullptr;
    delete node;
  } else {
    // case: node is a single black leaf
    if (root == node) {
      // node is root
      delete root;
      root = nullptr;
    } else {
      // node is any other black leaf.
      auto pivot = getBrother(node);
      if (node == node->parent->right_child)
        node->parent->right_child = nullptr;
      else
        node->parent->left_child = nullptr;
      delete node;
      // call rebalance with the deleted node;s brother as pivot
      balancePostRemove(pivot);
    }
  }
}

// returns the number of red children to current node
int rbTree::countRedChildren(rbNode* node) {
  int count = 0;
  if (node->left_child && !node->left_child->isBlack)
    count++;
  if (node->right_child && !node->right_child->isBlack)
    count++;
  return count;
}

// remove balance is passed the node whose brother has a deficiency. the
// subsequent rotations use this node as pivot
void rbTree::balancePostRemove(rbNode* pivot) {
  // nodes rebalanced according to Xcn rules given in lecture slides

  if (pivot->isBlack) {
    // cases: *b*
    int redCount = countRedChildren(pivot);
    if (redCount == 0 && pivot->parent->isBlack) {
      // case: Rb0 and Lb0 case 1
      pivot->isBlack = false;
      if (pivot->parent != root)
        balancePostRemove(getBrother(pivot->parent));
    } else if (redCount == 0 && !pivot->parent->isBlack) {
      // case: Rb0 and Lb0 case 2
      pivot->isBlack = false;
      pivot->parent->isBlack = true;
    } else {
      rbNode *pivot1, *passiveChild;
      // in subsequent*b* cases, one of the pivot's children act as the pivot
      // and the other as a passive child
      if (pivot == pivot->parent->left_child) {
        pivot1 = pivot->right_child;
        passiveChild = pivot->left_child;
      } else {
        pivot1 = pivot->left_child;
        passiveChild = pivot->right_child;
      }

      if (redCount == 1 && passiveChild && !passiveChild->isBlack) {
        // case: Rb1 and Lb1 case 1
        passiveChild->isBlack = true;
        pivot->isBlack = pivot->parent->isBlack;
        pivot->parent->isBlack = true;
        rotate(pivot);
      } else if (pivot1 && !pivot1->isBlack) {
        // case: Rb1 and Lb1 case 2 and
        // case: Rb2 and Lb2
        pivot1->isBlack = pivot->parent->isBlack;
        pivot->parent->isBlack = true;
        rotate(pivot1);
        rotate(pivot1);
      }
    }
  } else {
    // cases: *r*
    rbNode *pivot1, *pivot2, *passiveChild1;
    if (pivot->parent->left_child == pivot) {
      pivot1 = pivot->right_child;
      pivot2 = pivot1->right_child;
      passiveChild1 = pivot1->left_child;
    } else {
      pivot1 = pivot->left_child;
      pivot2 = pivot1->left_child;
      passiveChild1 = pivot1->right_child;
    }
    int redCount = countRedChildren(pivot1);

    if (redCount == 0) {
      // case: Rr0 and Lr0
      pivot1->isBlack = false;
      pivot->isBlack = true;
      rotate(pivot);
    } else if (redCount == 1 && passiveChild1 && !passiveChild1->isBlack) {
      // cases: Rr1 and Lr1 - case 1
      passiveChild1->isBlack = true;
      rotate(pivot1);
      rotate(pivot1);
    } else {
      // cases: Rr1 and Lr1 case2 and Rr2 and Lr2
      pivot2->isBlack = true;
      rotate(pivot2);
      rotate(pivot2);
      rotate(pivot2);
    }
  }
}

// function to locate the node closest to the given id.
// returns an exact match if found, otherwise returns the closest match
std::pair<bool, rbTree::rbNode*> rbTree::locate(int id) {
  rbNode* current = root;

  if (current == nullptr)
    return std::make_pair(false, nullptr);

  while (true) {
    if (id == current->index)
      // exact match, return the current node.
      return std::make_pair(true, current);
    else if (current->index < id && current->right_child != nullptr)
      // id of current node is less than query iterate to
      // right subtree if not null
      current = current->right_child;
    else if (current->index > id && current->left_child != nullptr)
      // id of current node is less than query iterate to
      // left subtree if not null
      current = current->left_child;
    else
      // not an exact match but nowhere else to iterate -
      // current node is best approximation.
      return std::make_pair(false, current);
  }
}

bool rbTree::has(int id) {
  auto element = locate(id);
  return element.first;
}

// returns the count stored in the node corresponding to the given id.
// if such a node is not found, 0 is returned
int& rbTree::at(int id) {
  auto element = locate(id);
  if (element.first == false) {
    throw "element not found";
  }
  return element.second->count;
}

// returns the id and count value for the node which is next to the one with id
// in in order traversald
std::pair<int, int> rbTree::next(int id) {
  auto element = locate(id);
  if (element.second == nullptr)  // no nodes in the tree
    return std::make_pair(0, 0);

  if (element.first == false && element.second->index > id)
    // node not found but returned node is closest successor
    return std::make_pair(element.second->index, element.second->count);
  else {
    auto successor = next(element.second);
    if (successor == nullptr)
      return std::make_pair(0, 0);
    else
      return std::make_pair(successor->index, successor->count);
  }
}

// returns the id and count value for the node which is before the one with id
// in in order traversald
std::pair<int, int> rbTree::previous(int id) {
  auto element = locate(id);
  if (element.second == nullptr)
    // no nodes in the tree
    return std::make_pair(0, 0);

  if (element.first == false && element.second->index < id)
    // node not found but returned node is the closest predecessor
    return std::make_pair(element.second->index, element.second->count);
  else {
    auto predecessor = previous(element.second);
    if (predecessor == nullptr)
      return std::make_pair(0, 0);
    else
      return std::make_pair(predecessor->index, predecessor->count);
  }
}

// returns the node which is next to the one with id in in order traversald
rbTree::rbNode* rbTree::next(rbNode* node) {
  if (node->right_child != nullptr) {
    return min(node->right_child);
  } else {
    // traverse up till a node is its parent's left child
    auto temp = node;
    while (temp->parent != nullptr && temp->index > temp->parent->index) {
      temp = temp->parent;
    }
    return temp->parent;
  }
}

// returns the node which is before the one with id in in order traversald
rbTree::rbNode* rbTree::previous(rbNode* node) {
  if (node->left_child != nullptr) {
    return max(node->left_child);
  } else {
    // traverse up till a node is its parent's right child
    auto temp = node;
    while (temp->parent != nullptr && temp->index < temp->parent->index) {
      temp = temp->parent;
    }
    return temp->parent;
  }
}

// returns the node with minmum id in the subtree whose root is the given node
rbTree::rbNode* rbTree::min(rbNode* node) {
  while (node->left_child != nullptr) {
    node = node->left_child;
  }
  return node;
}

// returns the node with maximum id in the subtree whose root is the given node
rbTree::rbNode* rbTree::max(rbNode* node) {
  while (node->right_child != nullptr) {
    node = node->right_child;
  }
  return node;
}
