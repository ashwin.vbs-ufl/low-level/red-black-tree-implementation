#include "algorithms.h"
#include "rbTree.h"

namespace RB {

// Dirty code, but this will be rewritten once iterators are implemented.
int sum_range(rbTree& tree, int id1, int id2) {
  if (id1 > id2)
    swap(id1, id2);

  int sum = tree.has(id1) ? tree.at(id1) : 0;
  int processed_id = id1;
  for (auto [id, count] = tree.next(processed_id); id <= id2;
       std::tie(id, count) = tree.next(processed_id)) {
    sum += count;
    processed_id = id;
  }
  return sum;
}

rbTree build_tree(int nodes, std::function<std::pair<int, int>()> getset) {
  rbTree tree;
  while (nodes > 0) {
    auto [id, count] = getset();
    tree.insert(id, count);
    nodes--;
  }
  return tree;
}

}  // namespace RB