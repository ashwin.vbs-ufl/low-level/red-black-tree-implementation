To guarantee memory safety, and for worry free programming, we ought to use smart pointers to manage the nodes. But each possible implementation comes with its own disadvantages.

### Requirements
1. Node reference - It should be possible to reference each node individually, usually achieved using an iterator or a pointer.
2. Tree traversibility in constant time per traversal - It should be possible, using the node reference, get a reference to its parent or children.
3. Reference validity after tree mutation - (This is not exactly a requirement, but a nice to have) - Node reference should be usable even after tree has been modified unless the referenced node itself has been deleted.

### Shared pointers
1. Node reference would be a weak ptr, as it would allow the node to be deleted.
2. Node could maintain shared ptrs to its children and keep a weak pointer to its parent allowing tree traversibility.
3. Weak pointers would be valid even after tree has been mutated.

But, using shared pointers comes with a significant overhead as the weak pointers would have to be resolved to shared pointer frequently.

### Unique pointers
Nodes will own their children as they will maintain unique pointers to them. But they have no way to refer to their parents.
1. There is no good way to refer to a node maintained by a unique ptr except to have a ptr or reference to the unique ptr itself. But as we see next, this is not enough.
2. Just being able to access a node cannot allow full traversal as the node itself does not have a reference to its parent. The reference would have to be a stack of references where the top of the stack is the current node and the bottom is the root. Popping the top off the stack would allow traversal to parent.
3. Any change to the tree that affects the nodes in this stack would invalidate it. It is possible to rebuild this, but it would be an operation of log(n) complexity.

This too is not an ideal solution.

### Raw pointers?
The solution is then to fallback to using raw pointers to manage the tree. Care has to be taken to be memory safe, but this is not impossible.


## Prior work:
STL implementation shipped with gcc and clang uses rb tree implemented using raw pointers to manage data in set and map containers.